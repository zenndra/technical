-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 23, 2019 at 08:36 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `technical`
--
CREATE DATABASE IF NOT EXISTS `technical` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `technical`;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE IF NOT EXISTS `barang` (
  `id` varchar(50) NOT NULL,
  `kode` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` decimal(15,2) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_at` date NOT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `kode`, `nama`, `jumlah`, `harga`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES
('69bacb33-83b2-4ea9-8d7c-f0698ac718bb', 'WMCYCLE', 'Will Fly', 5, '10000.00', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', '2019-09-24', NULL, NULL, NULL, NULL),
('755509b8-5a93-4897-b47d-ccedbefd481c', 'PLGN', 'Polygon', 10, '500000.00', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', '2019-09-24', NULL, NULL, NULL, NULL),
('94530e64-1c1e-4db2-80f8-f9fe2d138db6', 'string2', 'string', 1, '1234.56', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', '2019-09-22', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', '2019-09-22', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', '2019-09-22'),
('c9775464-b2fa-4e32-8d2c-d04b3c562cd9', 'OTL', 'Antik', 5, '15000000.00', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', '2019-09-22', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', '2019-09-24', NULL, NULL),
('dd7a3dc1-dc05-4247-8607-6b07f3ba9c09', 'tobedelete', 'string', 4, '123123.00', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', '2019-09-22', NULL, NULL, '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', '2019-09-22');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `nomor` varchar(15) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `created_at` date NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `nama`, `tanggal_lahir`, `nomor`, `alamat`, `email`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
('12accf5b-358a-43ee-b38a-194afa123f94', 'Zenzen', '1995-09-01', '082118801224', 'Berkoh', 'zenzen@gmail.com', '2019-09-22', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', '2019-09-24', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', '2019-09-22', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4'),
('27d1b4d0-6bc1-44f8-a02d-f6af782f9b9d', 'Lucy', '1994-09-22', '087719505253', 'Permata Hijau', 'lucy@gmail.com', '2019-09-22', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', '2019-09-24', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman`
--

CREATE TABLE IF NOT EXISTS `pengiriman` (
  `id` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `pengirim` varchar(50) DEFAULT NULL,
  `penerima` varchar(50) NOT NULL,
  `waktu_kirim` date DEFAULT NULL,
  `waktu_terima` date DEFAULT NULL,
  `created_at` date NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengiriman`
--

INSERT INTO `pengiriman` (`id`, `alamat`, `pengirim`, `penerima`, `waktu_kirim`, `waktu_terima`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
('3d804703-4b3e-4827-802e-f0231617fec5', 'string', 'Courier', 'string', NULL, NULL, '2019-09-23', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', NULL, NULL, NULL, NULL),
('698f38e2-ae55-4189-aaa0-0223a21b12e8', 'Dibawa sendiri', 'Dibawa sendiri', 'Dibawa sendiri', NULL, '2019-09-23', '2019-09-23', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', NULL, NULL, NULL, NULL),
('a233198b-c8e7-44ff-b1f2-54babda7ff67', 'string', 'Courier', 'string', NULL, NULL, '2019-09-23', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', NULL, NULL, NULL, NULL),
('d863e254-61ac-41c8-9ac7-909a04c0e52a', 'string', 'Courier', 'string', NULL, NULL, '2019-09-23', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE IF NOT EXISTS `transaksi` (
  `id` varchar(50) NOT NULL,
  `id_barang` varchar(50) NOT NULL,
  `id_member` varchar(50) DEFAULT NULL,
  `id_pengiriman` varchar(50) DEFAULT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `id_barang`, `id_member`, `id_pengiriman`, `jumlah`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
('d3d5f0b0-a1b9-44b2-a6b7-d0e7d31f89a0', 'c9775464-b2fa-4e32-8d2c-d04b3c562cd9', NULL, '698f38e2-ae55-4189-aaa0-0223a21b12e8', 2, '2019-09-23', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', NULL, NULL, NULL, NULL),
('06d577ce-9948-4aa8-bac6-104e96e29d1d', 'c9775464-b2fa-4e32-8d2c-d04b3c562cd9', NULL, 'a233198b-c8e7-44ff-b1f2-54babda7ff67', 3, '2019-09-23', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', NULL, NULL, NULL, NULL),
('c9d80327-79ee-4c8d-a0f1-d507681f9874', 'c9775464-b2fa-4e32-8d2c-d04b3c562cd9', '12accf5b-358a-43ee-b38a-194afa123f94', '3d804703-4b3e-4827-802e-f0231617fec5', 1, '2019-09-23', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', NULL, NULL, NULL, NULL),
('927ac661-0657-4e68-aef5-a83f61e52484', 'c9775464-b2fa-4e32-8d2c-d04b3c562cd9', '12accf5b-358a-43ee-b38a-194afa123f94', 'd863e254-61ac-41c8-9ac7-909a04c0e52a', 1, '2019-09-23', '5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `secret_key` varchar(50) NOT NULL,
  `created_at` date NOT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `secret_key`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
('5546d3f6-77ba-47cc-96f1-2a2ab9295bf4', 'indratesting', '$2a$10$UaK2GOEIZYiVQyPcdfy3Auup/Sd8eKY4cKIA6OmuSpZh4LGUj6JJm', 'MzM5ZTM0ZWItMmRhMy00M2JmLWEwYzQtM2U0N2QzMjcxOWYx', '2019-09-22', NULL, NULL, NULL, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
