package com.technical.technical.controller;

import com.technical.technical.constants.Constants;
import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.constants.TokenConstants;
import com.technical.technical.dto.MemberDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.factory.ServiceFac;
import com.technical.technical.service.MemberService;
import com.technical.technical.util.response.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/member")
public class MemberController {

    private Logger logger = LoggerFactory.getLogger(MemberController.class);

    @Autowired
    ServiceFac serviceFac;

    @RequestMapping(value = "/view/all", method = RequestMethod.GET)
    public ResponseEntity<RestResponse<List>> viewAllMember(@RequestHeader String Authorization) {
        RestResponse<List> result = new RestResponse<>();
        result.setSuccess(false);
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        try {
            MemberService memberService = serviceFac.getMemberService();
            result.setMessage("");
            result.setSuccess(true);
            result.setData(memberService.findAllMember());
            httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            logger.error("EXCEPTION", ex);
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            return new ResponseEntity<>(result, httpStatus);
        }
    }

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ResponseEntity<RestResponse<Map<String, Object>>> viewMember(@RequestHeader String Authorization, @RequestParam String idMember) {
        RestResponse<Map<String, Object>> result = new RestResponse<>();
        result.setSuccess(false);
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        try {
            MemberService memberService = serviceFac.getMemberService();
            result.setMessage("");
            result.setSuccess(true);
            result.setData(memberService.findMemberById(idMember));
            httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            logger.error("EXCEPTION", ex);
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            return new ResponseEntity<>(result, httpStatus);
        }
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<RestResponse<String>> createMember(@RequestHeader String Authorization, @RequestBody MemberDTO memberDTO, HttpServletRequest request) {
        HttpSession session = request.getSession();
        RestResponse<String> result = new RestResponse<>();
        result.setSuccess(false);
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        try {
            MemberService memberService = serviceFac.getMemberService(Constants.ACTION_INSERT, memberDTO);
            String userId = session.getAttribute(TokenConstants.CLAIM_USER_ID).toString();
            result.setMessage("");
            result.setSuccess(true);
            result.setData(memberService.insertMember(memberDTO, userId));
            httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (CustomException ex) {
            result.setMessage(ex.getMessage());
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            logger.error("EXCEPTION", ex);
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            return new ResponseEntity<>(result, httpStatus);
        }
    }

    @RequestMapping(value = "/edit", method = RequestMethod.PUT)
    public ResponseEntity<RestResponse<String>> editMember(@RequestHeader String Authorization, @RequestBody MemberDTO memberDTO, HttpServletRequest request) {
        HttpSession session = request.getSession();
        RestResponse<String> result = new RestResponse<>();
        result.setSuccess(false);
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        try {
            MemberService memberService = serviceFac.getMemberService(Constants.ACTION_UPDATE, memberDTO);
            String userId = session.getAttribute(TokenConstants.CLAIM_USER_ID).toString();
            result.setMessage("");
            result.setSuccess(true);
            result.setData(memberService.updateMember(memberDTO, userId));
            httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (CustomException ex) {
            result.setMessage(ex.getMessage());
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            logger.error("EXCEPTION", ex);
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            return new ResponseEntity<>(result, httpStatus);
        }
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResponseEntity<RestResponse<String>> deleteMember(@RequestHeader String Authorization, @RequestParam String idMember, HttpServletRequest request) {
        HttpSession session = request.getSession();
        RestResponse<String> result = new RestResponse<>();
        result.setSuccess(false);
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        try {
            MemberService memberService = serviceFac.getMemberService();
            String userId = session.getAttribute(TokenConstants.CLAIM_USER_ID).toString();
            result.setMessage("");
            result.setSuccess(true);
            result.setData(memberService.deleteMember(idMember, userId));
            httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (CustomException ex) {
            result.setMessage(ex.getMessage());
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            logger.error("EXCEPTION", ex);
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            return new ResponseEntity<>(result, httpStatus);
        }
    }
}
