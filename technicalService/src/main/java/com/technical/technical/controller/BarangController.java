package com.technical.technical.controller;

import com.technical.technical.constants.Constants;
import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.constants.TokenConstants;
import com.technical.technical.dto.BarangDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.factory.ServiceFac;
import com.technical.technical.service.BarangService;
import com.technical.technical.util.response.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/api/barang")
public class BarangController {

    private Logger logger = LoggerFactory.getLogger(BarangController.class);

    @Autowired
    ServiceFac serviceFac;

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ResponseEntity<RestResponse<Map<String, Object>>> viewBarang(@RequestHeader String Authorization, @RequestParam String idBarang) {
        RestResponse<Map<String, Object>> result = new RestResponse<>();
        result.setSuccess(false);
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        try {
            BarangService barangService = serviceFac.getBarangService();
            result.setMessage("");
            result.setSuccess(true);
            result.setData(barangService.viewBarang(idBarang, null));
            httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            logger.error("EXCEPTION", ex);
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            return new ResponseEntity<>(result, httpStatus);
        }
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<RestResponse<String>> createBarang(@RequestHeader String Authorization, @RequestBody BarangDTO barangDTO, HttpServletRequest request) {
        HttpSession session = request.getSession();
        RestResponse<String> result = new RestResponse<>();
        result.setSuccess(false);
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        try {
            BarangService barangService = serviceFac.getBarangServiceWithAction(Constants.ACTION_INSERT, barangDTO);
            String userId = session.getAttribute(TokenConstants.CLAIM_USER_ID).toString();
            result.setMessage("");
            result.setSuccess(true);
            result.setData(barangService.insertBarang(barangDTO, userId));
            httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (CustomException ex) {
            result.setMessage(ex.getMessage());
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            logger.error("EXCEPTION", ex);
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            return new ResponseEntity<>(result, httpStatus);
        }
    }

    @RequestMapping(value = "/edit", method = RequestMethod.PUT)
    public ResponseEntity<RestResponse<String>> editBarang(@RequestHeader String Authorization, @RequestBody BarangDTO barangDTO, HttpServletRequest request) {
        HttpSession session = request.getSession();
        RestResponse<String> result = new RestResponse<>();
        result.setSuccess(false);
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        try {
            BarangService barangService = serviceFac.getBarangServiceWithAction(Constants.ACTION_UPDATE, barangDTO);
            String userId = session.getAttribute(TokenConstants.CLAIM_USER_ID).toString();
            result.setMessage("");
            result.setSuccess(true);
            result.setData(barangService.updateBarang(barangDTO, userId));
            httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (CustomException ex) {
            result.setMessage(ex.getMessage());
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            logger.error("EXCEPTION", ex);
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            return new ResponseEntity<>(result, httpStatus);
        }
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResponseEntity<RestResponse<String>> deleteBarang(@RequestHeader String Authorization, @RequestParam String idBarang, HttpServletRequest request) {
        HttpSession session = request.getSession();
        RestResponse<String> result = new RestResponse<>();
        result.setSuccess(false);
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        try {
            BarangService barangService = serviceFac.getBarangService();
            String userId = session.getAttribute(TokenConstants.CLAIM_USER_ID).toString();
            result.setMessage("");
            result.setSuccess(true);
            result.setData(barangService.deleteBarang(idBarang, userId));
            httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (CustomException ex) {
            result.setMessage(ex.getMessage());
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            logger.error("EXCEPTION", ex);
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            return new ResponseEntity<>(result, httpStatus);
        }
    }

}
