package com.technical.technical.controller;


import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.constants.TokenConstants;
import com.technical.technical.dto.TransaksiDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.factory.ServiceFac;
import com.technical.technical.service.TransaksiService;
import com.technical.technical.util.response.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/transaksi")
public class TransaksiController {

    private Logger logger = LoggerFactory.getLogger(TransaksiController.class);

    @Autowired
    ServiceFac serviceFac;

    @RequestMapping(value = "/input", method = RequestMethod.POST)
    public ResponseEntity<RestResponse<String>> createMember(@RequestHeader String Authorization, @RequestBody TransaksiDTO.InputTransaksiDTO inputTransaksiDTO, HttpServletRequest request) {
        HttpSession session = request.getSession();
        RestResponse<String> result = new RestResponse<>();
        result.setSuccess(false);
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        try {
            TransaksiService transaksiService = serviceFac.getTransaksiServiceInput(inputTransaksiDTO);
            String userId = session.getAttribute(TokenConstants.CLAIM_USER_ID).toString();
            result.setMessage("");
            result.setSuccess(true);
            result.setData(transaksiService.inputTransaksi(inputTransaksiDTO, userId));
            httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (CustomException ex) {
            result.setMessage(ex.getMessage());
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            logger.error("EXCEPTION", ex);
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            return new ResponseEntity<>(result, httpStatus);
        }
    }
}
