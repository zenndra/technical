package com.technical.technical.controller;

import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.dto.RegisterAndLoginUserDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.factory.ServiceFac;
import com.technical.technical.service.BarangService;
import com.technical.technical.service.PublicService;
import com.technical.technical.util.response.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/public")
public class PublicController {

    @Autowired
    ServiceFac serviceFac;

    private Logger logger = LoggerFactory.getLogger(PublicController.class);

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<RestResponse<String>> register(@RequestBody RegisterAndLoginUserDTO dataRegister) {
        RestResponse<String> result = new RestResponse<>();
        result.setSuccess(false);
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        try {
            result.setMessage("");
            result.setSuccess(true);
            result.setData(serviceFac.getPublicServiceRegister(dataRegister).register(dataRegister));
            httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (CustomException ex) {
            result.setMessage(ex.getMessage());
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            logger.error("Exception", ex);
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            return new ResponseEntity<>(result, httpStatus);
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<RestResponse<Map<String, Object>>> login(@RequestBody RegisterAndLoginUserDTO dataLogin) {
        RestResponse<Map<String, Object>> result = new RestResponse<>();
        result.setSuccess(false);
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        try {
            PublicService publicService = serviceFac.getPublicService();
            result.setMessage("");
            result.setSuccess(true);
            result.setData(publicService.login(dataLogin));
            httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (CustomException ex) {
            result.setMessage(ex.getMessage());
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            logger.error("EXCEPTION", ex);
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            return new ResponseEntity<>(result, httpStatus);
        }
    }

    @RequestMapping(value = "/show/barang/active", method = RequestMethod.GET)
    public ResponseEntity<RestResponse<List<Map<String,Object>>>> showBarangActive() {
        RestResponse<List<Map<String,Object>>> result = new RestResponse<>();
        result.setSuccess(false);
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        try {
            BarangService barangService = serviceFac.getBarangService();
            result.setMessage("");
            result.setSuccess(true);
            result.setData(barangService.findAllActiveBarang());
            httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            logger.error("EXCEPTION", ex);
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            return new ResponseEntity<>(result, httpStatus);
        }
    }

}
