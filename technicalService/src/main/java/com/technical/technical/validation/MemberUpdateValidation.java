package com.technical.technical.validation;

import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.dto.MemberDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.service.MemberService;
import com.technical.technical.util.Util;
import com.technical.technical.validation.implementation.Specification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Optional;

public class MemberUpdateValidation implements Specification {
    private Logger logger = LoggerFactory.getLogger(MemberUpdateValidation.class);

    private MemberDTO memberDTO;
    private MemberService memberService;

    public MemberUpdateValidation(MemberDTO memberDTO, MemberService memberService) {
        this.memberDTO = memberDTO;
        this.memberService = memberService;
    }

    @Override
    public void mandatory() throws CustomException {
        if (Util.isNullOrEmpty(memberDTO.getId())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "id"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (Util.isNullOrEmpty(memberDTO.getNama())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "nama"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (Util.isNullOrEmpty(memberDTO.getNomor())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "nomor"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (Util.isNullOrEmpty(memberDTO.getEmail())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "email"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (Util.isNullOrEmpty(memberDTO.getAlamat())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "alamat"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (memberDTO.getTanggalLahir() == null) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "tanggal lahir"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
    }

    @Override
    public void validate() throws CustomException {
        mandatory();
        if (!memberService.isMemberExist(memberDTO.getId())) {
            logger.error(ErrorConstants.MEMBER_NOT_FOUND);
            throw new CustomException(ErrorConstants.MEMBER_NOT_FOUND);
        }
        if (memberService.isMemberRegistered(memberDTO.getNama(), memberDTO.getTanggalLahir())) {
            logger.error(ErrorConstants.REGISTERED_MEMBER);
            throw new CustomException(ErrorConstants.REGISTERED_MEMBER);
        }

    }
}
