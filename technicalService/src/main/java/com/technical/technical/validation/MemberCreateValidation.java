package com.technical.technical.validation;

import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.dto.MemberDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.service.MemberService;
import com.technical.technical.util.Util;
import com.technical.technical.validation.implementation.Specification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

public class MemberCreateValidation implements Specification {
    private Logger logger = LoggerFactory.getLogger(MemberCreateValidation.class);

    private MemberDTO memberDTO;
    private MemberService memberService;

    public MemberCreateValidation(MemberDTO memberDTO, MemberService memberService) {
        this.memberDTO = memberDTO;
        this.memberService = memberService;
    }

    @Override
    public void mandatory() throws CustomException {
        if (Util.isNullOrEmpty(memberDTO.getNama())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "nama"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (Util.isNullOrEmpty(memberDTO.getNomor())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "nomor"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (Util.isNullOrEmpty(memberDTO.getEmail())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "email"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (Util.isNullOrEmpty(memberDTO.getAlamat())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "alamat"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (memberDTO.getTanggalLahir() == null) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "tanggal lahir"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
    }

    @Override
    public void validate() throws CustomException {
        mandatory();
        if (memberService.isMemberRegistered(memberDTO.getNama(), memberDTO.getTanggalLahir())) {
            throw new CustomException(ErrorConstants.REGISTERED_MEMBER);
        }
    }
}
