package com.technical.technical.validation;

import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.constants.QueryConstants;
import com.technical.technical.dto.BarangDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.util.Util;
import com.technical.technical.validation.implementation.Specification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

public class BarangCreateValidation implements Specification {
    private Logger logger = LoggerFactory.getLogger(BarangCreateValidation.class);

    private BarangDTO barangDTO;
    private JdbcTemplate jdbcTemplate;

    public BarangCreateValidation(BarangDTO barangDTO, JdbcTemplate jdbcTemplate) {
        this.barangDTO = barangDTO;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void mandatory() throws CustomException {
        if (Util.isNullOrEmpty(barangDTO.getNama())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "nama"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (barangDTO.getHarga() == null) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "harga"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (barangDTO.getJumlah() == null) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "jumlah"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
    }

    @Override
    public void validate() throws CustomException {
        mandatory();
        String sql = String.format(QueryConstants.QUERY_CHECK_KODE_BARANG_IS_EXIST, barangDTO.getKode());
        if (!jdbcTemplate.queryForList(sql).isEmpty()) {
            logger.error(ErrorConstants.REGISTERED_KODE);
            throw new CustomException(ErrorConstants.REGISTERED_KODE);
        }
    }
}
