package com.technical.technical.validation;

import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.dto.BarangDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.service.BarangService;
import com.technical.technical.util.Util;
import com.technical.technical.validation.implementation.Specification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Map;
import java.util.Optional;

public class BarangUpdateValidation implements Specification {
    private Logger logger = LoggerFactory.getLogger(BarangUpdateValidation.class);

    private BarangDTO barangDTO;
    private JdbcTemplate jdbcTemplate;
    private BarangService barangService;

    public BarangUpdateValidation(BarangDTO barangDTO, JdbcTemplate jdbcTemplate, BarangService barangService) {
        this.barangDTO = barangDTO;
        this.jdbcTemplate = jdbcTemplate;
        this.barangService = barangService;
    }

    @Override
    public void mandatory() throws CustomException {
        if (Util.isNullOrEmpty(barangDTO.getId())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "id"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (Util.isNullOrEmpty(barangDTO.getNama())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "nama"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (barangDTO.getHarga() == null) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "harga"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (barangDTO.getJumlah() == null) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "jumlah"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
    }

    @Override
    public void validate() throws CustomException {
        mandatory();

        if (!barangService.isBarangExist(barangDTO.getId())) {
            logger.error(ErrorConstants.BARANG_NOT_FOUND);
            throw new CustomException(ErrorConstants.BARANG_NOT_FOUND);
        }
        Map<String, Object> findBarangById = barangService.viewBarang(barangDTO.getId(), null);
        if (!findBarangById.get("kode").equals(barangDTO.getKode())) {
            Map<String, Object> findBarangByKode = barangService.viewBarang(null, barangDTO.getKode());
            if (!findBarangByKode.isEmpty()) {
                logger.error(ErrorConstants.REGISTERED_KODE);
                throw new CustomException(ErrorConstants.REGISTERED_KODE);
            }
        }
    }
}
