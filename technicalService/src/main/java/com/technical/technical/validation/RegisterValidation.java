package com.technical.technical.validation;

import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.constants.QueryConstants;
import com.technical.technical.dto.RegisterAndLoginUserDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.util.Util;
import com.technical.technical.validation.implementation.Specification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class RegisterValidation implements Specification {
    private Logger logger = LoggerFactory.getLogger(RegisterValidation.class);

    private RegisterAndLoginUserDTO registerAndLoginUserDTO;
    private JdbcTemplate jdbcTemplate;

    public RegisterValidation(RegisterAndLoginUserDTO registerAndLoginUserDTO, JdbcTemplate jdbcTemplate) {
        this.registerAndLoginUserDTO = registerAndLoginUserDTO;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void mandatory() throws CustomException {
        if (Util.isNullOrEmpty(registerAndLoginUserDTO.getUsername())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "username"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (Util.isNullOrEmpty(registerAndLoginUserDTO.getPassword())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "password"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
    }

    @Override
    public void validate() throws CustomException {
        mandatory();
//        if (userRepository.findByUsername(registerAndLoginUserDTO.getUsername()).isPresent()) {
//            logger.error(ErrorConstants.REGISTERED_USERNAME);
//            throw new CustomException(ErrorConstants.REGISTERED_USERNAME);
//        }
        String sql = String.format(QueryConstants.QUERY_CHECK_USERNAME_IS_EXIST, registerAndLoginUserDTO.getUsername());
        List resultSet = jdbcTemplate.queryForList(sql);
        if (!resultSet.isEmpty()) {
            logger.error(ErrorConstants.REGISTERED_USERNAME);
            throw new CustomException(ErrorConstants.REGISTERED_USERNAME);
        }
    }
}
