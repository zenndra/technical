package com.technical.technical.validation;

import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.dto.TransaksiDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.service.BarangService;
import com.technical.technical.service.MemberService;
import com.technical.technical.util.Util;
import com.technical.technical.validation.implementation.Specification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Optional;

public class InputTransaksiValidation implements Specification {
    private Logger logger = LoggerFactory.getLogger(InputTransaksiValidation.class);

    private TransaksiDTO.InputTransaksiDTO inputTransaksiDTO;
    private MemberService memberService;
    private BarangService barangService;

    public InputTransaksiValidation(TransaksiDTO.InputTransaksiDTO inputTransaksiDTO, MemberService memberService, BarangService barangService) {
        this.inputTransaksiDTO = inputTransaksiDTO;
        this.memberService = memberService;
        this.barangService = barangService;
    }

    @Override
    public void mandatory() throws CustomException {
        if (Util.isNullOrEmpty(inputTransaksiDTO.getIdBarang())) {
            logger.error(ErrorConstants.NOTHING_TO_TRADE);
            throw new CustomException(ErrorConstants.NOTHING_TO_TRADE);
        }

        if (inputTransaksiDTO.getJumlah() == null || inputTransaksiDTO.getJumlah() == 0) {
            logger.error(ErrorConstants.NOTHING_TO_TRADE);
            throw new CustomException(ErrorConstants.NOTHING_TO_TRADE);
        }

        if (Util.isNullOrEmpty(inputTransaksiDTO.getIdMember()) && Util.isNullOrEmpty(inputTransaksiDTO.getAlamat())) {
            logger.error(ErrorConstants.NOT_MEMBER_AND_EMPTY_ALAMAT);
            throw new CustomException(ErrorConstants.NOT_MEMBER_AND_EMPTY_ALAMAT);
        }

        if (inputTransaksiDTO.getKirim() == null) {
            logger.error(ErrorConstants.NEED_TO_KIRIM_OR_NOT);
            throw new CustomException(ErrorConstants.NEED_TO_KIRIM_OR_NOT);
        }
        if (!Util.isNullOrEmpty(inputTransaksiDTO.getIdMember()) && inputTransaksiDTO.getAlamatMember() == null) {
            logger.error(ErrorConstants.KIRIM_MEMBER_OR_NOT);
            throw new CustomException(ErrorConstants.KIRIM_MEMBER_OR_NOT);
        }
    }

    @Override
    public void validate() throws CustomException {
        mandatory();
        if (!barangService.isBarangExist(inputTransaksiDTO.getIdBarang())) {
            logger.error(ErrorConstants.BARANG_NOT_FOUND);
            throw new CustomException(ErrorConstants.BARANG_NOT_FOUND);
        }
        Map<String, Object> barang = barangService.viewBarang(inputTransaksiDTO.getIdBarang(), null);
        if (barang.get("jumlah") == null || Integer.parseInt(barang.get("jumlah").toString()) == 0) {
            logger.error(ErrorConstants.BARANG_OUT_STOCK);
            throw new CustomException(ErrorConstants.BARANG_OUT_STOCK);
        }

        if (Integer.parseInt(barang.get("jumlah").toString()) < inputTransaksiDTO.getJumlah()) {
            logger.error(ErrorConstants.BARANG_STOCK_NOT_ENOUGH);
            throw new CustomException(ErrorConstants.BARANG_STOCK_NOT_ENOUGH);
        }

        if (!Util.isNullOrEmpty(inputTransaksiDTO.getIdMember())) {
            if (!memberService.isMemberExist(inputTransaksiDTO.getIdMember())) {
                logger.error(ErrorConstants.MEMBER_NOT_FOUND);
                throw new CustomException(ErrorConstants.MEMBER_NOT_FOUND);
            }
        }

        if (inputTransaksiDTO.getKirim()) {

        }
    }
}
