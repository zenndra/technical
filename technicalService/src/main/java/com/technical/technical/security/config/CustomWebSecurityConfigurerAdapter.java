package com.technical.technical.security.config;

import com.technical.technical.security.filter.TokenAuthorizationFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter.ReferrerPolicy;
import org.springframework.security.web.header.writers.StaticHeadersWriter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class CustomWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .headers()
                .contentTypeOptions()
                .and().xssProtection()
                .and().frameOptions().sameOrigin()
                .cacheControl()
                .and().referrerPolicy(ReferrerPolicy.NO_REFERRER)
                .and()
                .addHeaderWriter(new StaticHeadersWriter("Feature-Policy", "vibrate 'none'; geolocation 'none'"))
                .addHeaderWriter(new StaticHeadersWriter("X-Content-Security-Policy", "script-src 'self'"));

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/**").permitAll()
                .antMatchers("/api/public/**").permitAll()
                .antMatchers("/swagger*/**").permitAll()
                .antMatchers("/v2*/**").permitAll()
                .anyRequest()
                .authenticated()
        ;

        TokenAuthorizationFilter filter = new TokenAuthorizationFilter(authenticationManager(), getApplicationContext());

        http.addFilter(filter);

    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers(HttpMethod.OPTIONS, "/**")
        ;
    }
}