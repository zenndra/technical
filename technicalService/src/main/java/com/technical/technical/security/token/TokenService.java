package com.technical.technical.security.token;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

public class TokenService {

    private Logger logger = LoggerFactory.getLogger(TokenService.class);

    public static String createJwtToken(String issuer, Date expire, Date issuedAt, SignatureAlgorithm alg,
                                        String privKeyString, Map<String, String> claims) throws InvalidKeySpecException, NoSuchAlgorithmException {

        byte[] decodedKey = Base64.getDecoder().decode(privKeyString);
        Key originalKey = new SecretKeySpec(decodedKey, alg.getJcaName());
        JwtBuilder jwtBuilder = Jwts.builder().setIssuer(issuer).setExpiration(expire).setIssuedAt(issuedAt)
                .signWith(alg, originalKey);

        for (Entry<String, String> claim : claims.entrySet()) {
            jwtBuilder.claim(claim.getKey(), claim.getValue());
        }

        return jwtBuilder.compact();

    }

    public static Jws<Claims> parseJwsToken(String signingKeyBase64, String token)
            throws ExpiredJwtException, UnsupportedJwtException, MalformedJwtException, SignatureException, IllegalArgumentException {
        Jws<Claims> result = Jwts.parser().setSigningKey(signingKeyBase64).parseClaimsJws(token);
        return result;
    }
}
