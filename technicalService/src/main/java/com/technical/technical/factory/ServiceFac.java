package com.technical.technical.factory;

import com.technical.technical.constants.Constants;
import com.technical.technical.dto.BarangDTO;
import com.technical.technical.dto.MemberDTO;
import com.technical.technical.dto.RegisterAndLoginUserDTO;
import com.technical.technical.dto.TransaksiDTO;
import com.technical.technical.service.*;
import com.technical.technical.validation.*;
import com.technical.technical.validation.implementation.Specification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceFac {

    @Autowired
    DataSource dataSource;

    @Autowired
    PublicService publicService;

    JdbcTemplate jdbcTemplate;

    @Autowired
    BarangService barangService;

    @Autowired
    MemberService memberService;

    @Autowired
    TransaksiService transaksiService;

    public PublicService getPublicService() {
        return publicService;
    }

    public PublicService getPublicServiceRegister(RegisterAndLoginUserDTO registerAndLoginUserDTO) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        List<Specification> specificationList = new ArrayList<>();
        RegisterValidation registerValidation = new RegisterValidation(registerAndLoginUserDTO, jdbcTemplate);
        specificationList.add(registerValidation);
        publicService.setValidationManagers(specificationList);
        return publicService;
    }

    public BarangService getBarangService() {
        return barangService;
    }

    public BarangService getBarangServiceWithAction(String action, BarangDTO barangDTO) {
        List<Specification> specificationList = new ArrayList<>();
        jdbcTemplate = new JdbcTemplate(dataSource);
        if (action.equals(Constants.ACTION_INSERT)) {
            BarangCreateValidation barangCreateValidation = new BarangCreateValidation(barangDTO, jdbcTemplate);
            specificationList.add(barangCreateValidation);
        } else if (action.equals(Constants.ACTION_UPDATE)) {
            BarangUpdateValidation barangUpdateValidation = new BarangUpdateValidation(barangDTO, jdbcTemplate, getBarangService());
            specificationList.add(barangUpdateValidation);
        }
        barangService.setValidationManagers(specificationList);
        return barangService;
    }

    public MemberService getMemberService(String action, MemberDTO memberDTO) {
        List<Specification> specificationList = new ArrayList<>();
        if (action.equals(Constants.ACTION_INSERT)) {
            MemberCreateValidation memberCreateValidation = new MemberCreateValidation(memberDTO, getMemberService());
            specificationList.add(memberCreateValidation);
        } else if (action.equals(Constants.ACTION_UPDATE)) {
            MemberUpdateValidation memberUpdateValidation = new MemberUpdateValidation(memberDTO, getMemberService());
            specificationList.add(memberUpdateValidation);
        }
        memberService.setValidationManagers(specificationList);
        return memberService;
    }

    public MemberService getMemberService() {
        return memberService;
    }

    public TransaksiService getTransaksiService() {
        return transaksiService;
    }

    public TransaksiService getTransaksiServiceInput(TransaksiDTO.InputTransaksiDTO inputTransaksiDTO) {
        List<Specification> specificationList = new ArrayList<>();
        InputTransaksiValidation inputTransaksiValidation = new InputTransaksiValidation(inputTransaksiDTO, memberService, barangService);
        specificationList.add(inputTransaksiValidation);
        transaksiService.setValidationManagers(specificationList);
        return transaksiService;
    }
}
