package com.technical.technical.constants;

public class QueryConstants {


    public static final String ABSTRACT_AUDITING_ENTITY = "created_by, created_at, updated_by, updated_at, deleted_by, deleted_at ";

    public static final String QUERY_INSERT_BARANG = String.format("insert into barang(id, kode, nama, jumlah, harga, %s)", ABSTRACT_AUDITING_ENTITY);
    public static final String QUERY_INSERT_USER = String.format("insert into user(id, username, password, secret_key, %s)", ABSTRACT_AUDITING_ENTITY);
    public static final String QUERY_INSERT_MEMBER = "INSERT INTO member(id, nama, tanggal_lahir, nomor, alamat, " +
            "email, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by) " +
            "VALUES ('valueId','valueNama','valueTanggalLahir','valueNomor','valueAlamat','valueEmail',CURDATE(),'valueCreatedBy'," +
            "null,null," +
            "null,null)";
    public static final String QUERY_INSERT_TRANSAKSI = "INSERT INTO transaksi(id, id_barang, id_member, id_pengiriman, " +
            "jumlah, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by) " +
            "VALUES ('valueId','valueIdBarang','valueIdMember','valueIdPengiriman',valueJumlah," +
            "CURDATE(),'valueCreatedBy',null,null,null,null)";
    public static final String QUERY_INSERT_PENGIRIMAN = "INSERT INTO pengiriman(id, alamat, pengirim, penerima, waktu_kirim, " +
            "waktu_terima, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by) " +
            "VALUES ('valueId','valueAlamat','valuePengirim','valuePenerima',valueWaktuKirim," +
            "valueWaktuTerima,CURDATE(),'valueCreatedBy',null,null,null,null)";

    public static final String QUERY_UPDATE_BARANG = "UPDATE barang SET kode='valueKode',nama='valueNama',jumlah=valueJumlah," +
            "harga=valueHarga, updated_by=valueUpdatedBy,updated_at=valueUpdatedAt WHERE id=valueId";
    public static final String QUERY_BELI_BARANG = "UPDATE barang SET jumlah=valueJumlah WHERE id='valueId'";
    public static final String QUERY_DELETE_BARANG = "UPDATE barang SET deleted_by=valueDeletedBy,deleted_at=valueDeletedAt WHERE id=valueId";
    public static final String QUERY_UPDATE_MEMBER = "UPDATE member SET nama='valueNama',tanggal_lahir='valueTanggalLahir'," +
            "nomor='valueNomor', alamat='valueAlamat', email='valueEmail', " +
            "updated_by='valueUpdatedBy',updated_at=CURDATE() WHERE id='valueId'";
    public static final String QUERY_DELETE_MEMBER = "UPDATE member SET deleted_by=valueDeletedBy,deleted_at=valueDeletedAt WHERE id=valueId";


    public static final String QUERY_CHECK_USERNAME_IS_EXIST = "select * from user where username='%s' and deleted_at is null";
    public static final String QUERY_CHECK_KODE_BARANG_IS_EXIST = "select * from barang where kode='%s'";
    public static final String QUERY_CHECK_BARANG_IS_EXIST = "select * from barang where id='%s'";
    public static final String QUERY_CHECK_MEMBER_IS_EXIST = "select * from member where id='%s'";
    public static final String QUERY_CHECK_MEMBER_IS_REGISTERED = "select * from member where nama='%s' and tanggal_lahir='%s'";


    public static final String VALUE_ABSTRACT_AUDITING_ENTITY = "%s, %s, %s, %s, %s, %s";
    public static final String VALUE_ABSTRACT_AUDITING_ENTITY_INSERT = String.format(VALUE_ABSTRACT_AUDITING_ENTITY, "SELF SIGNED",
            "CURDATE()", "null", "null", "null", "null");
    public static final String VALUES_INSERT_BARANG = "'%s', '%s', '%s', %s, %s," + VALUE_ABSTRACT_AUDITING_ENTITY;
    public static final String VALUES_INSERT_USER = "'%s', '%s', '%s', '%s'," + VALUE_ABSTRACT_AUDITING_ENTITY;

}
