package com.technical.technical.constants;

import io.jsonwebtoken.SignatureAlgorithm;

public class TokenConstants {

    public static final SignatureAlgorithm TOKEN_ALG = SignatureAlgorithm.HS256;
    public static final String TOKEN_ISSUER = "Zenndra";
    public static final String CONST_HEADER_KEY_AUTHORIZATION = "Authorization";
    public static final String CONST_HEADER_VALUE_BEARER = "Bearer ";

    public static final String CLAIM_USER_ID = "userId";
    public static final String CLAIM_SECRET_KEY = "secretKey";
}
