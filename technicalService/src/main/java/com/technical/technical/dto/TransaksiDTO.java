package com.technical.technical.dto;

public class TransaksiDTO {

    public static final class InputTransaksiDTO {
        private String idBarang;
        private String idMember;
        private Integer jumlah;
        private Boolean kirim;
        private Boolean alamatMember;
        private String alamat;
        private String namaPembeli;

        public InputTransaksiDTO() {
        }

        public String getIdBarang() {
            return idBarang;
        }

        public void setIdBarang(String idBarang) {
            this.idBarang = idBarang;
        }

        public String getIdMember() {
            return idMember;
        }

        public void setIdMember(String idMember) {
            this.idMember = idMember;
        }

        public Integer getJumlah() {
            return jumlah;
        }

        public void setJumlah(Integer jumlah) {
            this.jumlah = jumlah;
        }

        public Boolean getKirim() {
            return kirim;
        }

        public void setKirim(Boolean kirim) {
            this.kirim = kirim;
        }

        public Boolean getAlamatMember() {
            return alamatMember;
        }

        public void setAlamatMember(Boolean alamatMember) {
            this.alamatMember = alamatMember;
        }

        public String getAlamat() {
            return alamat;
        }

        public void setAlamat(String alamat) {
            this.alamat = alamat;
        }

        public String getNamaPembeli() {
            return namaPembeli;
        }

        public void setNamaPembeli(String namaPembeli) {
            this.namaPembeli = namaPembeli;
        }
    }
}
