package com.technical.technical.dto;

public class RegisterAndLoginUserDTO {

    private String username;
    private String password;

    public RegisterAndLoginUserDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
