package com.technical.technical.util;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDate;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;


public class Util {

    public static BCryptPasswordEncoder getBCryptEncoder() {
        return new BCryptPasswordEncoder();
    }

    public static String generateRandomUUID() {
        return UUID.randomUUID().toString();
    }

    public static Date getNow() {
        return new Date();
    }

    public static LocalDate getLocalDateNow() {
        return LocalDate.now();
    }

    public static Date addTimeUnitToDate(Date current, int calendarUnit, int amount) {
        Calendar c = Calendar.getInstance();
        c.setTime(current);
        c.add(calendarUnit, amount);

        return c.getTime();
    }

    public static String generateRandomString(int length, boolean includeLetter, boolean includeNumber) {
        return RandomStringUtils.random(length, includeLetter, includeNumber);
    }

    public static String encodeBytes(byte[] data) {
        return Base64.getEncoder().encodeToString(data);
    }

    public static byte[] decodeBase64String(String data) {
        return Base64.getDecoder().decode(data);
    }

    public static String generateRandomStringInBase64() {
        return Base64.getEncoder().encodeToString(generateRandomUUID().getBytes());
    }

    public static Boolean isNullOrEmpty(String data) {
        return data == null || data.equals("");
    }
}
