package com.technical.technical.service;

import com.technical.technical.constants.Constants;
import com.technical.technical.constants.QueryConstants;
import com.technical.technical.dto.TransaksiDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.factory.ServiceFac;
import com.technical.technical.util.Util;
import com.technical.technical.validation.implementation.Specification;
import com.technical.technical.validation.implementation.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TransaksiService implements Validate {

    private Logger logger = LoggerFactory.getLogger(TransaksiService.class);

    List<Specification> validationmanagers;

    @Autowired
    private ServiceFac serviceFac;

    @Autowired
    DataSource dataSource;

    private JdbcTemplate jdbcTemplate;

    public List findAllMember() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "select * from transaksi";
        return jdbcTemplate.queryForList(sql);
    }

    @Transactional
    public String inputTransaksi(TransaksiDTO.InputTransaksiDTO inputTransaksi, String userId) throws CustomException {
        validate();
        BarangService barangService = serviceFac.getBarangService();
        MemberService memberService = serviceFac.getMemberService();

        barangService.beliBarang(inputTransaksi.getIdBarang(), inputTransaksi.getJumlah());
        String alamatKirim = null;
        String pengirim = null;
        String penerima = inputTransaksi.getNamaPembeli();
        String idMember = "null";
        Map<String, Object> member = new HashMap<>();
        if (!Util.isNullOrEmpty(inputTransaksi.getIdMember())) {
            idMember = inputTransaksi.getIdMember();
            member = memberService.findMemberById(inputTransaksi.getIdMember());
        }
        if (inputTransaksi.getKirim()) {
            if (inputTransaksi.getAlamatMember()) {
                if (!member.isEmpty()) {
                    alamatKirim = member.get("alamat").toString();
                    penerima = member.get("nama").toString();
                }
            } else {
                alamatKirim = inputTransaksi.getAlamat();
            }
            pengirim = Constants.COURIER;
        } else {
            pengirim = Constants.TAKE_IT_SELF;
            penerima = Constants.TAKE_IT_SELF;
            alamatKirim = Constants.TAKE_IT_SELF;
        }
        String idPengiriman = inputDataPengiriman(alamatKirim, pengirim, penerima, inputTransaksi.getKirim(), userId);

        String transaksiSql = QueryConstants.QUERY_INSERT_TRANSAKSI.replace("valueId", Util.generateRandomUUID())
                .replace("valueIdBarang", inputTransaksi.getIdBarang()).replace("valueIdMember", idMember)
                .replace("valueIdPengiriman", idPengiriman).replace("valueJumlah", inputTransaksi.getJumlah().toString())
                .replace("valueCreatedBy", userId);
        jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(transaksiSql);
        return "Transaksi Success";
    }

    private String inputDataPengiriman(String alamat, String pengirim, String penerima, Boolean kirim, String userId) {
        String id = Util.generateRandomUUID();
        String waktuTerima = "null";
        if (!kirim) {
            waktuTerima = "CURDATE()";
        }
        String pengirimanSql = QueryConstants.QUERY_INSERT_PENGIRIMAN.replace("valueId", id)
                .replace("valueAlamat", alamat).replace("valuePengirim", pengirim)
                .replace("valuePenerima", penerima).replace("valueWaktuKirim", "null")
                .replace("valueWaktuTerima", waktuTerima).replace("valueCreatedBy", userId);
        jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(pengirimanSql);
        return id;
    }

    @Override
    public void validate() throws CustomException {
        for (Specification specification : validationmanagers) {
            specification.validate();
        }
    }

    @Override
    public void setValidationManagers(List<Specification> validationmanagers) {
        this.validationmanagers = validationmanagers;
    }
}
