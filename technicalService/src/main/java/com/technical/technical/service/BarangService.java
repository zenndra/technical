package com.technical.technical.service;

import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.constants.QueryConstants;
import com.technical.technical.dto.BarangDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.util.Util;
import com.technical.technical.validation.implementation.Specification;
import com.technical.technical.validation.implementation.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class BarangService implements Validate {

    List<Specification> validationmanagers;

    @Autowired
    DataSource dataSource;

    private JdbcTemplate jdbcTemplate;

    public List<Map<String, Object>> findAllActiveBarang() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "select * from barang where deleted_at is null";
        return jdbcTemplate.queryForObject(sql, new RowMapper<List<Map<String, Object>>>() {
            @Override
            public List<Map<String, Object>> mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                List<Map<String, Object>> result = new ArrayList<>();
                while (resultSet.next()) {
                    Map<String, Object> row = new HashMap<String, Object>();
                    row.put("id", resultSet.getString("id"));
                    row.put("kode", resultSet.getString("kode"));
                    row.put("nama", resultSet.getString("nama"));
                    row.put("jumlah", resultSet.getInt("jumlah"));
                    row.put("harga", resultSet.getBigDecimal("harga"));
                    result.add(row);
                }
                return result;
            }
        });
    }

    public Map<String, Object> viewBarang(String idBarang, String kode) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        if (!Util.isNullOrEmpty(idBarang)) {
            idBarang = String.format("'%s'", idBarang);
        }
        if (!Util.isNullOrEmpty(kode)) {
            kode = String.format("'%s'", kode);
        }
        String sql = String.format("select * from barang where (id =%s or kode=%s) and deleted_at is null", idBarang, kode);
        return jdbcTemplate.queryForObject(sql, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                Map<String, Object> result = new HashMap<>();
                while (!resultSet.wasNull()) {
                    result.put("id", resultSet.getString("id"));
                    result.put("kode", resultSet.getString("kode"));
                    result.put("nama", resultSet.getString("nama"));
                    result.put("jumlah", resultSet.getInt("jumlah"));
                    result.put("harga", resultSet.getBigDecimal("harga"));
                    result.put("created_by", resultSet.getString("created_by"));
                    result.put("created_at", resultSet.getDate("created_at"));
                    result.put("updated_by", resultSet.getString("updated_by"));
                    result.put("updated_at", resultSet.getDate("updated_at"));
                    result.put("deleted_by", resultSet.getString("deleted_by"));
                    result.put("deleted_at", resultSet.getDate("deleted_at"));
                }
                return result;
            }
        });
    }

    public String insertBarang(BarangDTO barang, String userId) throws CustomException {
        validate();
        userId = String.format("'%s'", userId);
        String sql = QueryConstants.QUERY_INSERT_BARANG + "values (%s)";
        String values = String.format(QueryConstants.VALUES_INSERT_BARANG, Util.generateRandomUUID(), barang.getKode(), barang.getNama(), barang.getJumlah(), barang.getHarga(), userId, "CURDATE()", null, null, null, null);
        sql = String.format(sql, values);
        jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(sql);
        return "Insert Barang Success";
    }

    public String updateBarang(BarangDTO barang, String userId) throws CustomException {
        validate();
        userId = String.format("'%s'", userId);
        String idBarang = String.format("'%s'", barang.getId());
        String sql = QueryConstants.QUERY_UPDATE_BARANG.replace("valueKode", barang.getKode()).replace("valueNama", barang.getNama()).replace("valueJumlah", barang.getJumlah().toString()).replace("valueHarga", barang.getHarga().toString()).replace("valueUpdatedBy", userId).replace("valueUpdatedAt", "CURDATE()").replace("valueId", idBarang);
        jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(sql);
        return "Edit Barang Success";
    }

    public Boolean isBarangExist(String id) {
        String sql = String.format(QueryConstants.QUERY_CHECK_BARANG_IS_EXIST, id);
        jdbcTemplate = new JdbcTemplate(dataSource);
        if (!jdbcTemplate.queryForList(sql).isEmpty()) {
            return true;
        }
        return false;
    }

    public String deleteBarang(String idBarang, String userId) throws CustomException {
        userId = String.format("'%s'", userId);
        if (isBarangExist(idBarang)) {
            idBarang = String.format("'%s'", idBarang);
            String sql = QueryConstants.QUERY_DELETE_BARANG.replace("valueDeletedBy", userId).replace("valueDeletedAt", "CURDATE()").replace("valueId", idBarang);
            jdbcTemplate = new JdbcTemplate(dataSource);
            jdbcTemplate.update(sql);
            return "Delete Barang Success";
        } else {
            throw new CustomException(ErrorConstants.BARANG_NOT_FOUND);
        }
    }

    public void beliBarang(String idBarang, Integer jumlahBeli) {
        Map<String, Object> barang = viewBarang(idBarang, null);
        Integer sisaBarang = Integer.parseInt(barang.get("jumlah").toString()) - jumlahBeli;
        String sql = QueryConstants.QUERY_BELI_BARANG.replace("valueJumlah", sisaBarang.toString())
                .replace("valueId", idBarang);
        jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(sql);
    }

    @Override
    public void validate() throws CustomException {
        for (Specification specification : validationmanagers) {
            specification.validate();
        }
    }

    @Override
    public void setValidationManagers(List<Specification> validationmanagers) {
        this.validationmanagers = validationmanagers;
    }
}
