package com.technical.technical.service;

import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.constants.QueryConstants;
import com.technical.technical.dto.MemberDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.util.Util;
import com.technical.technical.validation.implementation.Specification;
import com.technical.technical.validation.implementation.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class MemberService implements Validate {

    private Logger logger = LoggerFactory.getLogger(MemberService.class);

    List<Specification> validationmanagers;

    @Autowired
    DataSource dataSource;

    private JdbcTemplate jdbcTemplate;

    public List findAllMember() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "select * from member";
        return jdbcTemplate.queryForList(sql);
    }

    public Map<String, Object> findMemberById(String idMember) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        if (!Util.isNullOrEmpty(idMember)) {
            idMember = String.format("'%s'", idMember);
        }
        String sql = String.format("select * from member where id =%s", idMember);
        return jdbcTemplate.queryForObject(sql, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                Map<String, Object> result = new HashMap<>();
                while (!resultSet.wasNull()) {
                    result.put("id", resultSet.getString("id"));
                    result.put("nama", resultSet.getString("nama"));
                    result.put("tanggalLahir", resultSet.getDate("tanggal_lahir"));
                    result.put("nomor", resultSet.getString("nomor"));
                    result.put("alamat", resultSet.getString("alamat"));
                    result.put("email", resultSet.getString("email"));
                    result.put("created_by", resultSet.getString("created_by"));
                    result.put("created_at", resultSet.getDate("created_at"));
                    result.put("updated_by", resultSet.getString("updated_by"));
                    result.put("updated_at", resultSet.getDate("updated_at"));
                    result.put("deleted_by", resultSet.getString("deleted_by"));
                }
                return result;
            }
        });
    }

    public String insertMember(MemberDTO member, String userId) throws CustomException {
        validate();
        String sql = QueryConstants.QUERY_INSERT_MEMBER.replace("valueNama", member.getNama()).
                replace("valueId", Util.generateRandomUUID()).
                replace("valueNama", member.getNama()).
                replace("valueTanggalLahir", member.getTanggalLahir().toString()).
                replace("valueNomor", member.getNomor()).
                replace("valueAlamat", member.getAlamat()).replace("valueEmail", member.getEmail()).
                replace("valueCreatedBy", userId);
        jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(sql);
        return "Create Member Success";
    }

    public String updateMember(MemberDTO member, String userId) throws CustomException {
        validate();
        String sql = QueryConstants.QUERY_UPDATE_MEMBER.replace("valueNama", member.getNama()).
                replace("valueTanggalLahir", member.getTanggalLahir().toString()).
                replace("valueNomor", member.getNomor()).
                replace("valueAlamat", member.getAlamat()).replace("valueEmail", member.getEmail()).
                replace("valueUpdatedBy", userId).replace("valueId", member.getId());
        jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(sql);
        return "Edit Member Success";
    }

    public Boolean isMemberExist(String id) {
        String sql = String.format(QueryConstants.QUERY_CHECK_MEMBER_IS_EXIST, id);
        jdbcTemplate = new JdbcTemplate(dataSource);
        if (!jdbcTemplate.queryForList(sql).isEmpty()) {
            return true;
        }
        return false;
    }

    public Boolean isMemberRegistered(String nama, LocalDate tanggalLahir) {
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String sql = String.format(QueryConstants.QUERY_CHECK_MEMBER_IS_REGISTERED, nama, tanggalLahir);
        jdbcTemplate = new JdbcTemplate(dataSource);
        if (!jdbcTemplate.queryForList(sql).isEmpty()) {
            return true;
        }
        return false;
    }

    public String deleteMember(String idMember, String userId) throws CustomException {
        userId = String.format("'%s'", userId);
        if (isMemberExist(idMember)) {
            idMember = String.format("'%s'", idMember);
            String sql = QueryConstants.QUERY_DELETE_MEMBER.replace("valueDeletedBy", userId).replace("valueDeletedAt", "CURDATE()").replace("valueId", idMember);
            jdbcTemplate = new JdbcTemplate(dataSource);
            jdbcTemplate.update(sql);
            return "Delete Member Success";
        } else {
            throw new CustomException(ErrorConstants.BARANG_NOT_FOUND);
        }
    }

    @Override
    public void validate() throws CustomException {
        for (Specification specification : validationmanagers) {
            specification.validate();
        }
    }

    @Override
    public void setValidationManagers(List<Specification> validationmanagers) {
        this.validationmanagers = validationmanagers;
    }
}
