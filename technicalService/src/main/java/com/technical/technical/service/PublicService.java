package com.technical.technical.service;

import com.technical.technical.constants.QueryConstants;
import com.technical.technical.constants.TokenConstants;
import com.technical.technical.dto.RegisterAndLoginUserDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.security.token.TokenService;
import com.technical.technical.util.Util;
import com.technical.technical.validation.implementation.Specification;
import com.technical.technical.validation.implementation.Validate;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Service
public class PublicService implements Validate {

    List<Specification> validationmanagers;

    @Autowired
    private DataSource dataSource;

    private JdbcTemplate jdbcTemplate;

    public String register(RegisterAndLoginUserDTO dataRegister) throws CustomException {
        validate();
        String sql = QueryConstants.QUERY_INSERT_USER + "values (%s)";
        String values = String.format(QueryConstants.VALUES_INSERT_USER,
                Util.generateRandomUUID(), dataRegister.getUsername(),
                Util.getBCryptEncoder().encode(dataRegister.getPassword()),
                Base64.getEncoder().encodeToString(Util.generateRandomUUID().getBytes()),
                null, "CURDATE()", null, null, null, null);
        sql = String.format(sql, values);
        jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(sql);
        return "Register success";
    }

    public Map<String, Object> login(RegisterAndLoginUserDTO dataLogin) throws CustomException, InvalidKeySpecException, NoSuchAlgorithmException {
        Map<String, Object> result = new HashMap<>();
        String sql = String.format("select * from user where username='%s' and deleted_at is null", dataLogin.getUsername());

        jdbcTemplate = new JdbcTemplate(dataSource);

        if (!jdbcTemplate.queryForList(sql).isEmpty()) {
            Map<String, Object> user = jdbcTemplate.queryForObject(sql, new RowMapper<Map<String, Object>>() {
                @Override
                public Map<String, Object> mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                    Map<String, Object> result = new HashMap<>();
                    result.put("id", resultSet.getString("id"));
                    result.put("password", resultSet.getString("password"));
                    result.put("secretKey", resultSet.getString("secret_key"));
                    return result;
                }
            });

            if (Util.getBCryptEncoder().matches(dataLogin.getPassword(), user.get("password").toString())) {
                Date now = Util.getNow();
                Date tokenExpired = Util.addTimeUnitToDate(now, Calendar.MINUTE, 15);
                SignatureAlgorithm alg = SignatureAlgorithm.HS256;
                Map<String, String> claims = new HashMap<>();
                claims.put(TokenConstants.CLAIM_USER_ID, user.get("id").toString());
                result.put("token", TokenService.createJwtToken(TokenConstants.TOKEN_ISSUER, tokenExpired, now, alg, user.get("secretKey").toString(), claims));
            } else {
                throw new CustomException("Password Invalid");
            }
        } else {
            throw new CustomException("Username not found");
        }

        return result;
    }

    public String getSecretKeyUser(String userId) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = String.format("select * from user where id='%s' and deleted_at is null", userId);
        if (!jdbcTemplate.queryForList(sql).isEmpty()) {
            return jdbcTemplate.queryForObject(sql, new RowMapper<String>() {
                @Override
                public String mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                    return resultSet.getString("secret_key");
                }
            });
        }
        return null;
    }

    @Override
    public void validate() throws CustomException {
        for (Specification specification : validationmanagers) {
            specification.validate();
        }
    }

    @Override
    public void setValidationManagers(List<Specification> validationmanagers) {
        this.validationmanagers = validationmanagers;
    }
}
